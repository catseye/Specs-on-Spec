Specs on Spec
=============

This is a collection of specifications for programming languages that have
not been implemented.  Indeed, many of them may well be unimplementable.

With the exception of TURKEY BOMB, the spec for which (I baldly
assert) was found unexpectedly one day under a stack of _Byte_ magazines at
a charity shop, these languages were designed by, and their specs written by,
Chris Pressey of Cat's Eye Technologies.  They are distributed under a
constellation of licenses which allow free redistribution; mostly
[CC-BY-ND 4.0](LICENSES/CC-BY-ND-4.0.txt), but see [LICENSES/](LICENSES/)
and [.reuse/dep5](.reuse/dep5) for more details. (This arrangement of
licensing info follows the [REUSE](https://reuse.software/) 3.0 convention.)

Here are some of the programming languages contained herein.

*   **[Oozlybub and Murphy](oozlybub-and-murphy/)**, a language which is
    only conjecturally Turing-complete.
*   **[Tamerlane](tamerlane/)**, a graph-rewriting language.
*   **[TURKEY BOMB](turkey-bomb/)**, the world's first and probably only
    programming-language-cum-drinking game.
*   **[You are Reading the Name of this Esolang](you-are-reading-the-name-of-this-esolang/)**,
    an esolang the name of which you were recently reading.

Also, I say "programming language", but of course that term is rather flexible
'round these parts:

*   **[Didigm](didigm/)** is a reflective cellular automaton;
*   **[Madison](madison/)** is a language for writing formal proofs;
*   **[MDPN](mdpn/)** is a (two-dimensional) parser-definition language; and
*   **[Opus-2](opus-2/)** is a "spoken" language, for some rather exceptional
    meaning of "speaking".

Most of these specifications are "finished" in the sense that there is nothing
obviously more to add to them.  (Of course, an implementation, or some really
brow-furrowing thought experiments, could always turn up problems with a
specification.)  The exceptions, which can be considered "works in progress",
are:
    
*   **[Irishsea](irishsea/)**, which is largely a set of sketchy notes for a
    livecoding language.
*   **[Sampo](sampo/)**, which is a loose collection of notes about features
    that would make for a "good" language that could be used in production.
*   **[PolyRical](polyrical/)**, which was an attempt at a successor language
    to SixtyPical, which will probably itself be superceded by something else.

### A Note on the Name of this Repository

In the dialect of English that's used where I come from, "spec" is short
for "specification" but "on spec" is short for "on *speculation*."  Thus the
name is referring to the idea that these language specifications were just
pulled out of thin air, rather than being products of deep consideration.
